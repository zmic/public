# wget -O install.ps1 https://bitbucket.org/zmic/public/raw/HEAD/install_pcbooking.ps1 && chmod 700 install.ps1 && . ./install.ps1

function Install_1
{
    # minimal gnome
    apt -y install gnome-session gnome-terminal 

    # LAMP
    apt -y install apache2
    apt -y install mysql-server

@"
    When the installation is finished, it’s recommended that you run a security script that comes pre-installed with MySQL. This script will remove some insecure default settings and lock down access to your database system. Start the interactive script by running:
"@
    #sudo mysql_secure_installation

    apt -y install php libapache2-mod-php php-mysql
    apt -y install p7zip-full p7zip-rar
    
    $deb = "mysql-workbench-community_8.0.22-1ubuntu20.04_amd64.deb" 
    wget https://dev.mysql.com/get/Downloads/MySQLGUITools/$deb
    apt -y install ./$deb
    rm $deb

}


function Download-String($url)
{
    (New-Object System.Net.WebClient).DownloadString($url)
}

function Download-File($url, $path)
{
    (New-Object System.Net.WebClient).DownloadFile($url, $path)
}

function Install-GuestAdditions
{
    $url = "http://download.virtualbox.org/virtualbox/"
    $html = Download-String $url
    [regex]$reg = '<a href="((\d+)\.(\d+)\.(\d+)/)">'
    $best = 0
    $match = $reg.match($html)
    while ($match.Success)
    {
        $a0 = ([int]$match.Groups[2].value) * 10000
        $b0 = ([int]$match.Groups[3].value) * 100
        $c0 = ([int]$match.Groups[4].value)
        if ( ($a0 + $b0 + $c0) -gt $best )
        {
            $best = ($a0 + $b0 + $c0)
            $name = $match.Groups[1].value
        }
        $match = $match.nextMatch()
    }

    $url = "http://download.virtualbox.org/virtualbox/" + $name
    write-output "Latest version: $url"

    $html = Download-String $url 

    $url = ""
    if ( $html -match 'A HREF="(VBoxGuestAdditions_.*?\.iso)">' )
    {
        $url += $matches[1]
        write-output $url
    }

    mkdir VBoxGuestAdditions
    cd VBoxGuestAdditions
    $path = (resolve-path ./VBoxGuestAdditions.iso).path
    Download-File $url $path
    7z x VBoxGuestAdditions.iso
    
    sudo apt -y install build-essential dkms linux-headers-$(uname -r)
    sudo sh ./VBoxLinuxAdditions.run --nox11
    
    cd ..
    rm -rf VBoxGuestAdditions
}


function Install_2
{

    if (-not (test-path ~/.ssh/key1))
    {
        #ssh-keygen -t rsa -b 4096 -C "key1" -f ~/.ssh/key1 -N \"\"
        ssh-keygen -t rsa -b 4096 -C "key1" -f ~/.ssh/key1 
    }
    cat /home/michael/.ssh/key1.pub
}

