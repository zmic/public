# wget -O install.sh https://bitbucket.org/zmic/public/raw/HEAD/install.sh && chmod 700 install.sh && sed -i -e 's/\r$//' install.sh && ./install.sh

sudo apt -y update
sudo apt -y upgrade
sudo apt -y install fail2ban

sudo apt -y install ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw enable

sudo apt install -y xfce4 xfce4-goodies xorg dbus-x11 x11-xserver-utils
sudo apt install -y xorgxrdp
sudo apt install -y xrdp
sudo ufw allow from 81.83.0.0/16 to any port 3389

sudo apt -y install snapd
sudo snap install powershell --classic
